#compose
dev:
	docker-compose -f docker-compose.yml up -d

prod:
	docker-compose -f docker-compose.production.yml up -d

down:
	docker-compose down


#ssh
ssh:
	ssh -i ~/.ssh/vscale_key root@84.38.183.138

copy-files:
	scp -r ./* root@84.38.183.138:/root/app

#example build for linux
#docker buildx build . --platform linux/amd64 -t mayatskiy/arsenal-pay:linux -f Dockerfile.production
#docker run -p 80:80 --rm -d  --name frontend mayatskiy/arsenal:prod

fix:
	npm run lintfix

lint:
	npm run lint
