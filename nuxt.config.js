export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Arsenal-pay',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
      {
        hid: 'msapplication-TileColor',
        name: 'msapplication-TileColor',
        content: '#FF6047',
      },
      {
        hid: 'msapplication-TileImage',
        name: 'msapplication-TileImage',
        content: 'icons/mstile-144x144.png',
      },
      {
        hid: 'msapplication-square70x70logo',
        name: 'msapplication-square70x70logo',
        content: 'icons/mstile-70x70.png',
      },
      {
        hid: 'msapplication-square150x150logo',
        name: 'msapplication-square150x150logo',
        content: 'icons/mstile-150x150.png',
      },
      {
        hid: 'msapplication-wide310x150logo',
        name: 'msapplication-wide310x150logo',
        content: 'icons/mstile-310x310.png',
      },
      {
        hid: 'msapplication-square310x310logo',
        name: 'msapplication-square310x310logo',
        content: 'icons/mstile-310x150.png',
      },
      {
        hid: 'application-name',
        name: 'application-name',
        content: 'ArsenalPay',
      },
      {
        hid: 'twitter:card',
        property: 'twitter:card',
        content: 'summary_large_image',
      },
      { hid: 'twitter:site', name: 'twitter:site', content: '@arsenalpay' },
      {
        hid: 'twitter:creator',
        name: 'twitter:creator',
        content: '@arsenalpay',
      },
      { hid: 'twitter:title', name: 'twitter:title', content: '' },
      { hid: 'twitter:description', name: 'twitter:description', content: '' },
      { hid: 'twitter:image', property: 'twitter:image', content: '' },
      { hid: 'vk:image', property: 'vk:image', content: '' },
      { hid: 'og:image', property: 'og:image', content: '' },
      {
        hid: 'og:image:secure_url',
        property: 'og:image:secure_url',
        content: '',
      },
      { hid: 'og:image:width', property: 'og:image:width', content: '1200' },
      { hid: 'og:image:height', property: 'og:image:height', content: '630' },
      { hid: 'og:image', property: 'og:image', content: '' },
      { hid: 'og:image:width', property: 'og:image:width', content: '540' },
      { hid: 'og:image:height', property: 'og:image:height', content: '540' },
      { hid: 'og:image:type', property: 'og:image:type', content: 'image/png' },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:site_name', property: 'og:site_name', content: 'ArsenalPay' },
      { hid: 'og:url', property: 'og:url', content: '' },
      { hid: 'og:title', property: 'og:title', content: '' },
      { hid: 'og:description', property: 'og:description', content: '' },
    ],
    link: [
      // { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: 'icons/favicon-16x16.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: 'icons/favicon-32x32.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '96x96',
        href: 'icons/favicon-96x96.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '120x120',
        href: 'icons/favicon-120x120.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '192x192',
        href: 'icons/android-icon-192x192.png',
      },
      { rel: 'apple-touch-icon', href: 'icons/touch-icon-iphone.png' },
      {
        rel: 'apple-touch-icon',
        sizes: '152x152',
        href: 'icons/touch-icon-ipad.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: 'icons/touch-icon-iphone-retina.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '167x167',
        href: 'icons/touch-icon-ipad-retina.png',
      },
      { rel: 'mask-icon', href: 'icons/website_icon.svg', color: '#FF6047' },
      {
        rel: 'preload',
        as: 'font',
        href: '/fonts/montserrat-light.woff2',
        type: 'font/woff2',
        crossorigin: 'anonymous',
      },
      {
        rel: 'preload',
        as: 'font',
        href: '/fonts/montserrat-medium.woff2',
        type: 'font/woff2',
        crossorigin: 'anonymous',
      },
      {
        rel: 'preload',
        as: 'font',
        href: '/fonts/montserrat-regular.woff2',
        type: 'font/woff2',
        crossorigin: 'anonymous',
      },
      {
        rel: 'preload',
        as: 'font',
        href: '/fonts/montserrat-light.woff',
        type: 'font/woff',
        crossorigin: 'anonymous',
      },
      {
        rel: 'preload',
        as: 'font',
        href: '/fonts/montserrat-medium.woff',
        type: 'font/woff',
        crossorigin: 'anonymous',
      },
      {
        rel: 'preload',
        as: 'font',
        href: '/fonts/montserrat-regular.woff',
        type: 'font/woff',
        crossorigin: 'anonymous',
      },
      { rel: 'stylesheet', href: '/fonts/fonts.css', type: 'text/css' },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'element-ui/lib/theme-chalk/index.css',
    'normalize.css/normalize.css',
    // 'swiper/swiper-bundle.css',
    // 'swiper/css/navigation',
    // 'swiper/css/pagination',
    // 'swiper/css/scrollbar',
    'plyr/dist/plyr.css',
    '~/assets/scss/app.scss',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['@/plugins/element-ui'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    '@nuxtjs/svg-sprite',
  ],

  svgSprite: {
    // manipulate module options
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
  ],

  styleResources: {
    scss: ['./assets/scss/*.scss'],
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [/^element-ui/],
    // standalone: true
  },
}
